module.exports = function getPositions(word, letter) {
  return word.split('').reduce((prev, currentLetter, idx) => (
    letter === currentLetter ? [...prev, idx] : prev
  ), []);
}
