const randomInt = require('random-int');

module.exports = function pickRandomFromArray(array) {
  return array[randomInt(0, array.length - 1)];
}
